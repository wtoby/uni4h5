import Vue from 'vue'
import App from './App'
/* 导入vant-ui */
import Vant from 'vant';
import 'vant/lib/index.less';
Vue.use(Vant);

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
