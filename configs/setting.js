const SETTING = {}

/**
 * 网络相关
 */
SETTING.baseUrl = 'https://easydoc.xyz/mock/2WvHLkNL'

/**
 * 用户相关
 */
SETTING.tokenKey = "logintoken"
SETTING.tokenValue = "xdsedeswe2323" //如果token是通过请求服务器获取的，此时设置为空字符串
SETTING.name = ""

export default SETTING