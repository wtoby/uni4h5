/**
 * 网络通信类工具
 * http请求/下载/上传
 */
import config from '../configs/setting.js'
import Common from './Common.js'
import User from './User.js'

export default class {

	/**
	 * 统一成功处理
	 * @param {Object} resolve
	 * @param {Object} reject
	 * @param {Object} res
	 * @param {Object} options
	 */
	static _success(resolve, reject, res, options) {
		let httpCode = res.statusCode.toString();
		if (httpCode.startsWith("2")) {
			resolve(res.data.data);
		} else {
			// Todo 应用错误处理
			this._showError(res.data.msg)
			reject(res);
		}
	}

	/**
	 * 统一失败处理
	 * @param {Object} reject
	 * @param {Object} err
	 */
	static _fail(reject, err) {
		// Todo 服务器错误处理
		this._showError()
	}

	/**
	 * 显示错误信息
	 * @param {Object} msg
	 */
	static _showError(msg) {
		uni.showToast({
			title: msg || '服务器出现一个未知错误',
			duration: 2000,
			icon: 'none'
		})
	}

	/**
	 * 网络请求
	 */
	static _request({
		url,
		header,
		data,
		sslVerify = true,
		method = 'GET',
		showLoading = true
	}) {

		return new Promise((resolve, reject) => {

			if (showLoading) {
				uni.showLoading({
					title: '正在加载...',
					mask: true
				});
			}

			const REQUEST_PARAMS = {};

			// #ifdef APP-PLUS
			if (uni.getSystemInfoSync().platform == 'android') {
				REQUEST_PARAMS.sslVerify = sslVerify;
			}
			// #endif

			REQUEST_PARAMS.url = config.baseUrl + url;
			REQUEST_PARAMS.method = method;
			REQUEST_PARAMS.header = {};
			REQUEST_PARAMS.header[config.tokenKey] = User.getToken();

			if (Common.isObj(header)) {
				REQUEST_PARAMS.header = Object.assign(REQUEST_PARAMS.header, header);
			}

			if (data != null) {
				REQUEST_PARAMS.data = data;
			}

			REQUEST_PARAMS.success = (res) => {
				showLoading && uni.hideLoading();
				this._success(resolve, reject, res, REQUEST_PARAMS);
			};

			REQUEST_PARAMS.fail = (err) => {
				this._fail(reject, err);
			};

			REQUEST_PARAMS.complete = () => {
				showLoading && uni.hideLoading();
			}

			uni.request(REQUEST_PARAMS);
		})

	}

	static get(url, data, showLoading = true) {

		return this._request({
			url: url,
			data: data,
			method: 'GET',
			header: {
				'content-type': 'application/x-www-form-urlencoded'
			},
			showLoading: showLoading
		});

	}

	static postJson(url, data, showLoading = true) {

		return this._request({
			url: url,
			data: data,
			method: 'POST',
			header: {
				'content-type': 'application/json'
			},
			showLoading: showLoading
		});

	}

	static post(url, data, showLoading = true) {

		return this._request({
			url: url,
			data: data,
			method: 'POST',
			header: {
				'content-type': 'application/x-www-form-urlencoded'
			},
			showLoading: showLoading
		});
	}
}
