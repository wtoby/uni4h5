import setting from '../configs/setting.js'

export default class {
	
	/**
	 * token相关
	 */
	static getToken(){
		try{
			return setting.tokenValue || uni.getStorageSync(setting.tokenKey);
		}catch(e){
			console.error("获取token失败");
		}
	}
	
	static setToken(val){
		try {
		    uni.setStorageSync(setting.tokenKey, val);
		} catch (e) {
		    console.error("设置token失败");
		}
	}
	
	static removeToken(){
		try {
		    uni.removeStorageSync(setting.tokenKey);
		} catch (e) {
		    console.error("删除token失败");
		}
	}
	
	/**
	 * 用户相关
	 */
	static getUserInfo(){
		try{
			return uni.getStorageSync(setting.userKey);
		}catch(e){
			console.error("获取用户信息失败");
		}
	}
	
	static setUserInfo(val){
		try {
		    uni.setStorageSync(setting.userKey, val);
		} catch (e) {
		    console.error("设置用户信息失败");
		}
	}
	
	static removeUserInfo(){
		try {
		    uni.removeStorageSync(setting.userKey);
		} catch (e) {
		    console.error("删除用户信息失败");
		}
	}
}