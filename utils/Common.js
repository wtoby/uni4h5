/**
 * 通用工具类
 */
export default class{
	
	/**
	 * 类型判断
	 */
	static isObj(obj){
		return Object.prototype.toString.call(obj) === '[object Object]'?true:false;
	}
	
	
}
